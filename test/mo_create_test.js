const Student = require("../app/student");
const assert = require("assert"); // Provided by Mocha

describe("Create Records Test!", () => {
    it("Create a user in DB", () => {
        // assert(true);
        const sam = new Student({"name": "Sam", age: 12});
        sam.save()
            .then((resp) => {
                assert(!sam.isNew);
            })
            .catch((err) => console.log(err));
    });
});


// All Read Tests 

describe("Read Records Test", () => {
    let reader;
    beforeEach((done) => {
        reader = new Student({"name": "SumanAc", age: 12})
        reader.save()
            .then(() => {
                done();
            })
            .catch((err) => console.log(err));
    });
    
    it("Fetching Records from DB: reader", (done) => {
        Student.find({"name": "SumanAc"})
            .then((respArr) => {
                assert(respArr[0]._id.toString() === reader._id.toString());
                done();
            })
            .catch(err => {
                console.log(err)
            });
    })
});


// All Delete Tests

describe("Delete Records Test", () => {
    let deleter;
    beforeEach((done) => {
        deleter = new Student({"name": "SumanAC", age: 12});
        deleter.save()
            .then(() => {
                done();
            })
            .catch((err) => logger.error(err));
    });
    it("Deleting Records from DB: deleter", (done) => {
        Student.findOneAndDelete(deleter._id)
            .then(() => {
                return Student.findOne({"name": "SumanAC"})
            })
            .then((resp) => {
                assert(resp === null);
                done();
            });
    });
});


// All Update Tests

describe("Update Records Test", () => {
    let updater;
    beforeEach((done) => {
        updater = new Student({"name": "SUMANACHARYYA", age: 12});
        updater.save()
            .then(() => {
                done();
            })
            .catch((err) => logger.error(err));
    });
    it("Set and Save the Records!", (done) => {
        updater.set("name", "SUMANACHARYYA-UPDATER");
        updater.save()
            .then(() => {
                return Student.find({});
            })
            .then((resp) => {
                console.log(resp);
                assert(resp[0].name !== "SUMANACHARYYA");
                assert(resp[0].name === "SUMANACHARYYA-UPDATER");
                done();
            });
    });
});