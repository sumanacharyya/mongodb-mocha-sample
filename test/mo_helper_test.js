const mongoose = require('mongoose');
mongoose.Promise = global.Promise; // Using ES6 Promises

mongoose.set('strictQuery', true);


before((done) => {
    mongoose.connect("mongodb://localhost:27017/mongotube");

    mongoose.connection
        .once("open", () => {
            // console.log("MongoDB Connected!");
            done();
        })
        .on("error", (err) => console.log(err));
});

beforeEach((done) => {
    mongoose.connection.collections.students.drop(() => {
        done();
    })
})