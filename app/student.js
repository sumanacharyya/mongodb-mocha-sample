const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// const StudentSchema = new Schema({
//     name: {
//         type: 'string',
//     },
// });
const StudentSchema = new Schema({
    name: {
        type: 'string',
        required: true,
    },
    age: {
        type: Number,
        required: true,
    },
});

const Student = mongoose.model("student", StudentSchema);

// EXPORTS:

module.exports = Student;